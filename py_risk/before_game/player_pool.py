from py_risk.game.game_logic.online_player import OnlinePlayerType, OnlinePlayer
from py_risk.game.game_logic.player import Player
from py_risk.game_asssets import Color


class PlayerPool:

    def __init__(self):
        self.player_pool = []
        self.selected_players = []

        self._init_player_pool()

    def _init_player_pool(self):
        for color in Color.PLAYER_COLORS:
            player = Player(color)
            self.player_pool.append(player)

    def push_player(self):
        player = self.player_pool.pop(-1)
        self.selected_players.append(player)
        return player

    def pop_player(self):
        player = self.selected_players.pop(-1)
        self.player_pool.append(player)

    def get_max_number_of_player(self):
        return len(self.player_pool) + len(self.selected_players)

    def get_players(self) -> list:
        return self.selected_players


class OnlinePlayerPool(PlayerPool):

    def _init_player_pool(self):
        self.player_pool.append(OnlinePlayer(Color.PLAYER_COLORS[0], OnlinePlayerType.local))
        for color in Color.PLAYER_COLORS[1:]:
            player = OnlinePlayer(color, OnlinePlayerType.remote)
            self.player_pool.append(player)

    def remove_player(self, player: OnlinePlayer):
        self.selected_players.remove(player)
        self.player_pool.append(player)
