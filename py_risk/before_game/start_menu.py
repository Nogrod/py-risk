import pygame
import pygame_gui
from pygame_gui.core.interfaces import IUIManagerInterface

from py_risk.before_game.MainMenuInterface import MainMenuInterface, MainMenuState
from py_risk.before_game.host_game_menu import HostGameMenu
from py_risk.before_game.single_player_menu import SinglePlayerMenu
from py_risk.before_game.player_pool import PlayerPool
from py_risk.game.view.button_menu import ButtonMenu


class ModeMenu(ButtonMenu, MainMenuInterface, MainMenuState):

    def __init__(self, ui_manager: IUIManagerInterface, player_pool: PlayerPool):
        menu_button_names = ["singleplayer", "host_game", "join_game"]
        menu_with = 160
        button_distance = 20
        button_height = int(menu_with * 0.2)
        button_size = (int(menu_with * 0.8), button_height)
        self.player_pool = player_pool
        super().__init__(ui_manager, menu_button_names, menu_with, button_distance, button_size)

    def handle_input(self, event: pygame.event.Event) -> MainMenuState:
        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == self.elements['singleplayer']:
                    self.kill()
                    return SinglePlayerMenu(self.ui_manager, self.player_pool)
                elif event.ui_element == self.elements['host_game']:
                    self.kill()
                    return HostGameMenu(self.ui_manager, self.player_pool)
                elif event.ui_element == self.elements['join_game']:
                    pass  # todo multiplayer part nicht in diesem branch umgesetzt

        return self

