import os

import pygame

from py_risk.before_game.StartGame import StartGameState
from py_risk.before_game.player_pool import PlayerPool
from py_risk.before_game.start_menu import ModeMenu
from py_risk.helper import load_image, exit_game
from py_risk.game_info import GameInformation


def run(game_info: GameInformation):
    window = game_info.window_surface
    ui_manager = game_info.ui_manager
    player_pool = PlayerPool()
    state = ModeMenu(ui_manager, player_pool)

    background = load_image(os.path.join("data", "background.jpg"), game_info.window_size)

    clock = pygame.time.Clock()
    is_running = True
    while is_running:
        time_delta = clock.tick(60) / 1000.0
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit_game()
            ui_manager.process_events(event)
            state = state.handle_input(event)

            if isinstance(state, StartGameState):
                is_running = False
                player = state.get_player()
                break

        window.blit(background, (0, 0))
        ui_manager.update(time_delta)
        ui_manager.draw_ui(window)

        pygame.display.update()

    game_info.player = player
