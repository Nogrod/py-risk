import pygame
import pygame_gui

from py_risk.before_game.MainMenuInterface import MainMenuState
from py_risk.before_game.StartGame import StartGameState
from py_risk.before_game.player_panel import PlayerGUI
from py_risk.before_game.player_pool import PlayerPool
from py_risk.game.view.button_menu import ElementMenu
from py_risk.localization import get_localization


class SinglePlayerMenu(ElementMenu, MainMenuState):
    def __init__(self, ui_manager, player_pool: PlayerPool):
        super().__init__(ui_manager)
        self.player_pool = player_pool
        self.player_gui_s = []
        self.current_number_of_player = 0

        buttons = [("add_player_button", get_localization("add_player")),
                   ("remove_player_button", get_localization("remove_player")),
                   ("start_game_button", get_localization("start_game"))]

        menu_with = 160
        label_distance = 20
        label_height = int(menu_with * 0.2)
        label_size = (int(menu_with * 0.8), label_height)

        self.menu_panel = self.create_menu(self.player_pool.get_max_number_of_player() + len(buttons), label_distance, label_height, menu_with, 1)

        for i in range(self.player_pool.get_max_number_of_player()):
            self._add_player_label(label_size, 2, str(i), label_distance)

        for button_name, button_text in buttons:
            self.add_button(label_size, 2, button_name, button_text, label_distance)

        self._add_player()
        self._add_player()

    def _add_player_label(self, button_size: tuple, layer: int, element_name: str, element_distance: int):
        relative_rect = self.get_next_relative_rect(element_distance, button_size, self.menu_panel)

        self.elements[element_name] = PlayerGUI(self.ui_manager, relative_rect, self.menu_panel, layer)

    def kill(self):
        self.menu_panel.kill()
        for element in self.elements.values():
            element.kill()

    def handle_input(self, event: pygame.event.Event):
        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == self.elements["remove_player_button"]:
                    self._remove_player()
                elif event.ui_element == self.elements["add_player_button"]:
                    self._add_player()
                elif event.ui_element == self.elements["start_game_button"] and self.current_number_of_player >= 2:
                    self.kill()
                    return StartGameState(self.player_pool)

        return self

    def _add_player(self):
        if self.current_number_of_player < self.player_pool.get_max_number_of_player():
            player = self.player_pool.push_player()
            self.elements[str(self.current_number_of_player)].set_player(player)
            self.current_number_of_player += 1

    def _remove_player(self):
        if self.current_number_of_player > 1:
            self.player_pool.pop_player()
            self.current_number_of_player -= 1
            self.elements[str(self.current_number_of_player)].remove_player()
