import pygame
import pygame_gui

from py_risk.game.game_logic.player import Player


class PlayerGUI:

    def __init__(self, ui_manager, relative_rect: pygame.Rect, container, layer: int):
        self.panel = pygame_gui.elements.UILabel(relative_rect=relative_rect,
                                                 text="",
                                                 manager=ui_manager,
                                                 container=container,
                                                 anchors={'left': 'left',
                                                          'right': 'right',
                                                          'top': 'top',
                                                          'bottom': 'bottom'})
        self.panel.change_layer(layer)

    def set_player(self, player: Player):
        self.panel.set_text(player.get_name())

    def remove_player(self):
        self.panel.set_text("")

    def kill(self):
        self.panel.kill()