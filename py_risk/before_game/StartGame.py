from py_risk.before_game.MainMenuInterface import MainMenuState
from py_risk.before_game.player_pool import PlayerPool


class StartGameState(MainMenuState):

    def __init__(self, player_pool: PlayerPool):
        self.player_pool = player_pool

    def get_player(self):
        return self.player_pool.get_players()
