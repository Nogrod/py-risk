from __future__ import annotations

import abc

import pygame


class MainMenuInterface(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def handle_input(self, event: pygame.event.Event) -> MainMenuState:
        pass


class MainMenuState:
    pass
