import pygame
import pygame_gui
from pygame_gui.core.interfaces import IUIManagerInterface


def load_image(path: str, target_image_size: tuple = None, convert_alpha=False) -> pygame.Surface:
    if target_image_size is None:
        display_info = pygame.display.Info()
        target_image_size = (display_info.current_w, display_info.current_h)
    image = pygame.image.load(path)
    if convert_alpha:
        image = image.convert_alpha()
    else:
        image = image.convert()
    color_id_map_scaled = pygame.transform.scale(image, target_image_size)

    return color_id_map_scaled


def get_ui_image(path: str, target_size: tuple, ui_manager: IUIManagerInterface, ui_level=0,
                 convert_alpha=False) -> pygame_gui.elements.UIImage:
    image = load_image(path, target_size, convert_alpha)
    ui_image = pygame_gui.elements.UIImage(image.get_rect(), image, ui_manager)
    ui_image.change_layer(ui_level)

    return ui_image


def exit_game():
    pygame.quit()
    exit(0)
