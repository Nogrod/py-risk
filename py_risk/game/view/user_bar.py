import pygame
import pygame_gui
from pygame_gui.core.interfaces import IUIManagerInterface

from py_risk.game.Event import NextPhaseEvent
from py_risk.game.game_logic.turn_logic.turn import Turn
from py_risk.game.view.gird_panel import GridPanel
from py_risk.game.view.ui_state import GameState
from py_risk.localization import get_localization


class InformationPanel(GridPanel):

    def __init__(self, ui_manager: IUIManagerInterface, turn: Turn, relative_rect: pygame.Rect):
        self.ui_manager = ui_manager
        self.turn = turn

        container = pygame_gui.elements.UIPanel(relative_rect, 0, ui_manager)
        container.hide()

        num_elements = 4
        solider_label_rect = self._transform_relative_rect(relative_rect, num_elements, 1)
        reinforcement_rect = self._transform_relative_rect(relative_rect, num_elements, 2)
        card_count_rect = self._transform_relative_rect(relative_rect, num_elements, 3)
        show_cards_rect = self._transform_relative_rect(relative_rect, num_elements, 4)

        self.solider_label = pygame_gui.elements.UILabel(solider_label_rect, "", ui_manager, container=container)
        self.reinforcement_label = pygame_gui.elements.UILabel(reinforcement_rect, "", ui_manager, container=container)
        self.card_count_label = pygame_gui.elements.UILabel(card_count_rect, "", ui_manager, container=container)

        self.show_cards_button = pygame_gui.elements.UIButton(show_cards_rect,  get_localization('show_cards'), ui_manager, container)
        self.show_cards_button.show()

        self.solider_label.show()
        self.reinforcement_label.show()
        self.card_count_label.show()

    def update(self):
        self.solider_label.set_text(f"{get_localization('solider_count')}: {self.turn.get_solider_number()}")
        reinforcement_quantity = self.turn.get_reinforcement_quantity()
        if reinforcement_quantity == 0:
            reinforcement_text = ""
        else:
            reinforcement_text = f"{get_localization('reinforcement_count')}: {reinforcement_quantity}"
        self.reinforcement_label.set_text(reinforcement_text)

        card_manager = self.turn.get_current_card_manager()
        self.card_count_label.set_text(f"{get_localization('num_cards')}: {card_manager.get_number_of_cards()}")


class TurnPanel(GridPanel):
    def __init__(self, ui_manager: IUIManagerInterface, turn: Turn, relative_rect: pygame.Rect):
        self.ui_manager = ui_manager
        self.turn = turn

        container = pygame_gui.elements.UIPanel(relative_rect, 0, ui_manager)
        container.hide()

        num_elements = 3
        current_phase_rect = self._transform_relative_rect(relative_rect, num_elements, 1)
        current_player_rect = self._transform_relative_rect(relative_rect, num_elements, 2)
        next_turn_rect = self._transform_relative_rect(relative_rect, num_elements, 3)

        self.current_phase_label = pygame_gui.elements.UILabel(current_phase_rect, "", ui_manager, container=container)
        self.current_player_label = pygame_gui.elements.UILabel(current_player_rect, "", ui_manager,
                                                                container=container)

        self.next_turn_button = pygame_gui.elements.UIButton(next_turn_rect, get_localization("next_phase"), ui_manager, container)
        self.next_turn_button.show()

        self.current_phase_label.show()
        self.current_player_label.show()

    def update(self):
        self.current_player_label.set_text(f"{get_localization('current_player')}: {self.turn.get_current_player_name()}")
        self.current_phase_label.set_text(f"{get_localization('current_phase')}: {self.turn.get_current_phase_name()}")


class UserBar:

    def __init__(self, ui_manager: IUIManagerInterface, information_panel: InformationPanel, turn_panel: TurnPanel):
        self.ui_manager = ui_manager
        self.information_panel = information_panel
        self.turn_panel = turn_panel

    def handle_input(self, event: pygame.event.Event) -> GameState:
        if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
            if event.ui_element == self.information_panel.show_cards_button:
                return GameState.HAND_CARD_STATE
            if event.ui_element == self.turn_panel.next_turn_button:
                event = NextPhaseEvent()
                event.post()
                return GameState.GAME_STATE
        else:
            return GameState.GAME_STATE

    def update(self):
        self.information_panel.update()
        self.turn_panel.update()
