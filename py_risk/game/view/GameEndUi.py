import pygame
import pygame_gui
from pygame_gui.core.interfaces import IUIManagerInterface

from py_risk.game.game_logic.player import Player
from py_risk.game.view.button_menu import ElementMenu
from py_risk.game.view.gui_element import GuiElement
from py_risk.game.view.ui_state import GameState
from py_risk.helper import exit_game
from py_risk.localization import get_localization


class GameEndUi(ElementMenu, GuiElement):
    def __init__(self, ui_manager: IUIManagerInterface):
        super().__init__(ui_manager)
        self.ui_manager = ui_manager

        menu_with = 160
        label_distance = 20
        label_height = int(menu_with * 0.2)
        label_size = (int(menu_with * 0.8), label_height)

        self.menu_panel = self.create_menu(2, label_distance, label_height, menu_with, 2)
        self._add_winner_label(label_size, "winner", label_distance)
        self.add_button(label_size, 3, "game_end_button", get_localization("exit"), label_distance)

        self.hide()

    def _add_winner_label(self, label_size: tuple, element_name: str, element_distance: int):
        relative_rect = self.get_next_relative_rect(element_distance, label_size, self.menu_panel)

        label = pygame_gui.elements.UILabel(relative_rect,
                                            "",
                                            self.ui_manager,
                                            self.menu_panel)
        label.change_layer(3)
        self.elements[element_name] = label

    def kill(self):
        pass

    def hide(self):
        self.menu_panel.hide()
        for element in self.elements.values():
            element.hide()

    def show(self, winner: Player):
        self.menu_panel.show()
        for element in self.elements.values():
            element.show()
        self.elements["winner"].set_text(f"{get_localization('winner')}: {winner.get_name()}")

    def handle_input(self, event: pygame.event.Event) -> GameState:
        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == self.elements["game_end_button"]:
                    exit_game()
        return GameState.GAME_END
