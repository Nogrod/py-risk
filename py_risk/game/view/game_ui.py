import pygame
from pygame_gui.core.interfaces import IUIManagerInterface

from py_risk.game.Event import SoldierCountEvent, NextPhaseEvent
from py_risk.game.game_logic.turn_logic.turn import Turn
from py_risk.game.view.game_map_gui import GameMapGUI
from py_risk.game.view.gui_element import GuiElement
from py_risk.game.view.ui_state import GameState
from py_risk.game.view.user_bar import UserBar


class GameUi(GuiElement):

    def __init__(self, ui_manager: IUIManagerInterface, turn: Turn, world_ui: GameMapGUI, user_bar: UserBar):
        self.ui_manager = ui_manager
        self.turn = turn
        self.world_ui = world_ui
        self.user_bar = user_bar

    def handle_input(self, event: pygame.event.Event) -> GameState:
        self.world_ui.process_events(event)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                self.hide()
                return GameState.MENU_STATE
            if event.key == pygame.K_n:
                event = NextPhaseEvent()
                event.post()
        elif event.type == pygame.USEREVENT:
            return self.user_bar.handle_input(event)
        elif event.type == pygame.MOUSEWHEEL:
            event = SoldierCountEvent(event.y)
            event.post()
        return GameState.GAME_STATE

    def hide(self):
        pass

    def show(self):
        pass

    def kill(self):
        pass

    def draw(self, window: pygame.Surface):
        self.world_ui.draw(window)
        self.user_bar.update()
