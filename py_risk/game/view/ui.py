import pygame

from py_risk.game.game_logic.player import Player
from py_risk.game.view.GameEndUi import GameEndUi
from py_risk.game.view.game_ui import GameUi
from py_risk.game.view.hand_ui import HandCardUIHandler
from py_risk.game.view.menu_handler import MenuHandler
from py_risk.game.view.ui_state import GameState


class UI:
    state: GameState

    def __init__(self, game_ui: GameUi, menu_handler: MenuHandler, hand_card_ui: HandCardUIHandler,
                 gamed_end_ui: GameEndUi, background: pygame.Surface):
        self.background = background
        self.state = GameState.MENU_STATE
        self.game_ui = game_ui
        self.menu_ui = menu_handler
        self.hand_card_ui = hand_card_ui
        self.game_end_ui = gamed_end_ui

    def handle_input(self, event: pygame.event.Event):
        if self.state == GameState.MENU_STATE:
            new_state = self.menu_ui.handle_input(event)
        elif self.state == GameState.GAME_STATE:
            new_state = self.game_ui.handle_input(event)
        elif self.state == GameState.HAND_CARD_STATE:
            new_state = self.hand_card_ui.handle_input(event)
        elif self.state == GameState.GAME_END:
            new_state = self.game_end_ui.handle_input(event)

        if new_state != self.state:
            self.state = new_state
            if self.state == GameState.HAND_CARD_STATE:
                self.hand_card_ui.update()

    def draw(self, window: pygame.Surface):
        window.blit(self.background, (0, 0))
        if self.state == GameState.GAME_STATE:
            self.game_ui.draw(window)
        elif self.state == GameState.HAND_CARD_STATE:
            self.hand_card_ui.draw()

    def end_game(self, winner: Player):
        self.game_end_ui.show(winner)
        self.state = GameState.GAME_END
