import abc

import pygame


class GridPanel(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def update(self):
        raise NotImplemented

    @staticmethod
    def _transform_relative_rect(relative_rect: pygame.Rect, num_elements: int, factor: int):
        new_rect = relative_rect.copy()
        new_rect.height = int(relative_rect.height / num_elements)
        new_rect.bottomleft = (0, int(factor * relative_rect.height / num_elements))
        return new_rect