import pygame
from pygame_gui.core.interfaces import IUIManagerInterface

from py_risk.game.view.main_menu import GameMenu
from py_risk.game.view.ui_state import UIState, GameState


class MenuHandler:
    ui_state: UIState

    def __init__(self, ui_manager: IUIManagerInterface):
        self.ui_manager = ui_manager
        self.ui_state = GameMenu(ui_manager)

    def handle_input(self, event: pygame.event.Event):
        if self.ui_state is None:
            self.ui_state = GameMenu(self.ui_manager)

        self.ui_state = self.ui_state.handle_input(event)

        if self.ui_state is None:
            return GameState.GAME_STATE
        else:
            return GameState.MENU_STATE
