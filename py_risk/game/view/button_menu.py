import abc
import os
from abc import ABC

import pygame
import pygame_gui
from pygame_gui.core.interfaces import IUIManagerInterface

from py_risk.game.view.ui_state import UIState
from py_risk.helper import get_ui_image
from py_risk.game.view.gui_element import GuiElement
from py_risk.localization import get_localization


class ElementMenu(UIState, GuiElement, abc.ABC):
    @abc.abstractmethod
    def __init__(self, ui_manager: IUIManagerInterface):
        self.ui_manager = ui_manager
        self.elements = dict()

    def create_menu(self, num_elements: int, element_distance: int, element_height: int, menu_with: int, layer: int):
        display_info = pygame.display.Info()
        center_point = (int(display_info.current_w / 2), int(display_info.current_h / 2))

        menu_height = self._get_menu_height(num_elements, element_distance, element_height)

        menu_panel_layout = pygame.Rect(0, 0, menu_with, menu_height)
        menu_panel_layout.center = center_point

        menu_panel = pygame_gui.elements.UIPanel(manager=self.ui_manager,
                                                 object_id="main_menu",
                                                 relative_rect=menu_panel_layout,
                                                 starting_layer_height=0)
        menu_panel.change_layer(layer)
        return menu_panel

    @staticmethod
    def _get_menu_height(num_elements: int, distance_between_elements: int, element_height: int):
        elements_distance = (num_elements - 1) * distance_between_elements
        elements_height = num_elements * element_height
        distance_to_corner = 2 * distance_between_elements

        menu_height = elements_height + distance_to_corner + elements_distance
        return menu_height

    def get_next_relative_rect(self, element_distance: int, element_size: tuple, container):
        element_y_position = (len(self.elements) + 1) * element_distance + element_size[1] * len(self.elements)
        menu_rect = container.get_relative_rect()
        relative_rect = pygame.Rect(0, 0, * element_size)
        relative_rect.midtop = (menu_rect.width / 2, element_y_position)

        return relative_rect

    def add_button(self, button_size: tuple, layer: int, element_name: str, text: str, button_distance: int):
        button_layout_rect = self.get_next_relative_rect(button_distance, button_size, self.menu_panel)

        button = pygame_gui.elements.UIButton(relative_rect=button_layout_rect,
                                              text=text,
                                              manager=self.ui_manager,
                                              container=self.menu_panel,
                                              anchors={'left': 'left',
                                                       'right': 'right',
                                                       'top': 'top',
                                                       'bottom': 'bottom'})
        button.change_layer(layer)

        self.elements[element_name] = button

    @abc.abstractmethod
    def kill(self):
        pass


class ButtonMenu(ElementMenu, ABC):

    @abc.abstractmethod
    def __init__(self, ui_manager: IUIManagerInterface, menu_button_names: list, menu_with: int, button_distance: int,
                 button_size: tuple):
        super().__init__(ui_manager)
        overlay_img_path = os.path.join("data", "overlay.png")
        self.overlay = get_ui_image(path=overlay_img_path, target_size=None, ui_manager=ui_manager, convert_alpha=True,
                                    ui_level=2)
        self.show_menu = True
        self.menu_panel = self.create_menu(len(menu_button_names), button_distance, button_size[1], menu_with, layer=3)

        for button in menu_button_names:
            button_name = get_localization(button)
            self.add_button(button_size, 4, f"{button}", button_name, button_distance)

    def kill(self):
        self.menu_panel.kill()
        self.overlay.kill()
