import abc

import pygame

from py_risk.game.view.ui_state import GameState


class GuiElement(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def kill(self):
        raise NotImplemented

    @abc.abstractmethod
    def handle_input(self, event: pygame.event.Event) -> GameState:
        raise NotImplemented
