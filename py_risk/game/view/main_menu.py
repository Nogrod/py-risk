import pygame
import pygame_gui
from pygame_gui.core.interfaces import IUIManagerInterface

from py_risk.game.view.button_menu import ButtonMenu
from py_risk.helper import exit_game


class GameMenu(ButtonMenu):

    def __init__(self, ui_manager: IUIManagerInterface):
        menu_button_names = ["continue", "exit"]
        menu_with = 160
        button_distance = 20
        button_height = int(menu_with * 0.2)
        button_size = (int(menu_with * 0.8), button_height)
        super().__init__(ui_manager, menu_button_names, menu_with, button_distance, button_size)

    def handle_input(self, event: pygame.event.Event):
        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == self.elements["continue"]:
                    self.kill()
                    return None
                if event.ui_element == self.elements["exit"]:
                    exit_game()

        return self
