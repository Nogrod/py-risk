from __future__ import annotations

import abc
from enum import Enum

import pygame


class UIState(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def handle_input(self, event: pygame.event.Event) -> UIState:
        raise NotImplemented


class GameState(Enum):
    MENU_STATE = 0
    GAME_STATE = 1
    HAND_CARD_STATE = 2
    GAME_END = 3


class MainMenuState(Enum):
    CHOOSE_GAME_MODE = 0
    CHOOSE_PLAYER = 1
    START_GAME = 2
