import pygame
import pygame_gui
from pygame_gui.core.interfaces import IUIManagerInterface

from py_risk.game.Event import SelectCardEvent, ActivateCardsEvent
from py_risk.game.game_logic.card_logic.card_manager import CardManager
from py_risk.game.game_logic.turn_logic.turn import Turn
from py_risk.game.game_logic.card_logic.card import Card
from py_risk.game.view.ui_state import GameState, UIState
from py_risk.localization import get_localization


class HandCardUI:
    activate_button: pygame_gui.elements.UIButton

    def __init__(self, turn: Turn, ui_manager: IUIManagerInterface, relative_rect: pygame.Rect):
        self.turn = turn
        self.card_manager = turn.get_current_card_manager()
        cards = self.card_manager.get_cards()
        self.container = pygame_gui.elements.UIPanel(relative_rect, -1, ui_manager)
        self.container.hide()
        self.ui_cards = []
        self.activate_button = None

        self.build_ui(cards, ui_manager)

    def kill(self):
        for card_ui in self.ui_cards:
            card_ui.kill()
        self.activate_button.kill()

    def build_ui(self, cards: list, ui_manager: IUIManagerInterface):
        container_rect = self.container.relative_rect
        cards_with = container_rect.width * 0.1
        cards_height = container_rect.height * 0.4

        x_start = cards_with
        y_pos = container_rect.height / 2

        button_rect = pygame.Rect((0, 0), (cards_with * 1.2, cards_with * 0.3))
        button_rect.midbottom = (container_rect.width / 2, container_rect.height * 0.4)

        self.activate_button = pygame_gui.elements.UIButton(button_rect, get_localization("activate_cards"), ui_manager, self.container,
                                                            starting_height=10)
        self.activate_button.show()
        self.update_activate_button()

        for i, card in enumerate(cards):
            x_card_pos = i * cards_with * 1.1 + x_start
            card_rect = pygame.Rect((x_card_pos, y_pos), (cards_with, cards_height))
            ui_card = CardUI(card, card_rect, ui_manager, self.card_manager)
            self.ui_cards.append(ui_card)

    def process_event(self, event: pygame.event.Event):
        for ui_card in self.ui_cards:
            if ui_card.card_pressed(event):
                ui_card.select_card()
                return GameState.HAND_CARD_STATE

        if event.ui_element == self.activate_button:
            event = ActivateCardsEvent()
            event.post()
            self.kill()
            return GameState.GAME_STATE
        return GameState.HAND_CARD_STATE

    def draw(self):
        for ui_card in self.ui_cards:
            ui_card.update_text()

    def update_activate_button(self):
        if self.card_manager.can_activate_set() and self.turn.can_activate_hand_cards():
            self.activate_button.enable()
        else:
            self.activate_button.disable()


class HandCardUIHandler(UIState):
    hand_card_ui: HandCardUI

    def __init__(self, turn: Turn, ui_manager: IUIManagerInterface, show_cards_button: pygame_gui.elements.UIButton,
                 game_map: pygame.Rect):
        self.turn = turn
        self.ui_manager = ui_manager
        self.hand_card_ui = None
        self.show_cards_button = show_cards_button
        self.game_map = game_map

    def handle_input(self, event: pygame.event.Event) -> GameState:
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                return self._change_to_game_state()

        self.hand_card_ui.update_activate_button()

        if event.type == pygame.USEREVENT:
            if event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                if event.ui_element == self.show_cards_button:
                    return self._change_to_game_state()
                elif event.user_type == pygame_gui.UI_BUTTON_PRESSED:
                    return self.hand_card_ui.process_event(event)
        return GameState.HAND_CARD_STATE

    def _change_to_game_state(self) -> GameState:
        self.hand_card_ui.kill()
        return GameState.GAME_STATE

    def update(self):
        self.hand_card_ui = HandCardUI(self.turn, self.ui_manager, self.game_map)

    def draw(self):
        self.hand_card_ui.draw()


class CardUI:

    def __init__(self, card: Card, relative_rect: pygame.Rect, ui_manager: IUIManagerInterface,
                 card_manager: CardManager):
        self.card = card
        self.card_manager = card_manager

        self.button = pygame_gui.elements.UIButton(relative_rect, "", ui_manager)

        card_type_rect = relative_rect.copy()
        card_type_rect.height = card_type_rect.height / 3
        self.card_type_label = pygame_gui.elements.UILabel(card_type_rect, card.get_card_type_name(), ui_manager)

        territory_rect = card_type_rect.copy()
        territory_rect.y = card_type_rect.y + card_type_rect.height
        self.territory_label = pygame_gui.elements.UILabel(territory_rect, card.get_territory_name(), ui_manager)

        selected_rect = territory_rect.copy()
        selected_rect.y = territory_rect.y + territory_rect.height
        self.selected_label = pygame_gui.elements.UILabel(selected_rect, "", ui_manager)

    def kill(self):
        self.button.kill()
        self.card_type_label.kill()
        self.territory_label.kill()
        self.selected_label.kill()

    def update_text(self):
        if self.card_manager.is_card_selected(self.card):
            self.selected_label.set_text(get_localization("cards_selected"))
        else:
            self.selected_label.set_text("")

    def select_card(self):
        event = SelectCardEvent(self.card)
        event.post()

    def card_pressed(self, event: pygame.event.Event) -> bool:
        if event.ui_element == self.button:
            return True
        return False
