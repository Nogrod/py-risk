from __future__ import annotations

import os
import pygame

from py_risk.game.Event import TerritoryClickEvent
from py_risk.game.game_logic.territory import Territory
from py_risk.game_asssets import Fonts, Color
from py_risk.helper import load_image


class GameMapGUI:
    color_id_map: pygame.Surface

    def __init__(self, map_size: tuple, territories: dict):
        self.territories = territories

        color_id_map_path = os.path.join("data", "risko-grau-ohne-zahlen.png")
        self.color_id_map = load_image(color_id_map_path, map_size)
        self.map_rect = self.color_id_map.get_rect()

    def draw(self, window: pygame.Surface):
        window.blit(self.color_id_map, (0, 0))
        for territory in self.territories.values():
            territory.draw_territory(window)
        for territory in self.territories.values():
            territory.draw_unit_count(window)

    def process_events(self, event: pygame.event.Event):
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            self._process_button_click()

    def _mouse_position_to_territory_id(self, mouse_position: tuple) -> int:
        mouse_color = self.color_id_map.get_at(mouse_position)
        territory_id = mouse_color[0] - 100
        return territory_id

    def _process_button_click(self):
        mouse_position = pygame.mouse.get_pos()
        if self.map_rect.collidepoint(mouse_position):
            territory_id = self._mouse_position_to_territory_id(mouse_position)
            if 0 < territory_id <= len(self.territories):
                event = TerritoryClickEvent(territory_id)
                event.post()


class TerritoryGui:

    def __init__(self, id_: int, territory: Territory):
        self.id_ = id_
        self.territory = territory

    def draw_territory(self, window: pygame.Surface):
        territory_image = self.territory.get_image()
        window.blit(territory_image, (0, 0))

    def draw_unit_count(self, window: pygame.Surface):
        unit_label = Fonts.monospace_18.render(str(self.territory.solider_number), True, Color.WHITE)
        unit_label_rect = unit_label.get_rect()

        relative_circle_position = self.territory.get_center_position()
        circle_radius = 1.0 * max(unit_label_rect.width, unit_label_rect.height) / 2

        pygame.draw.circle(window, Color.DARK_GREY, relative_circle_position, circle_radius)
        window.blit(unit_label, self._get_label_position(unit_label))

    def _get_label_position(self, label: pygame.Surface) -> pygame.Rect:
        label_rect = label.get_rect()
        label_rect.center = self.territory.get_center_position()
        return label_rect
