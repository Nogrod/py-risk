from py_risk.game.game_logic.player import Player
from py_risk.game.game_logic.territory_image import TerritoryImage
from py_risk.game_asssets import Color
from py_risk.localization import get_localization


class Territory:

    def __init__(self, territory_id: int, image: TerritoryImage):
        self.id_ = territory_id
        self.border_territories = []
        self.solider_number = 1
        self.name = get_localization(self.id_)

        self.image = image
        self.player: Player = None

    def add_border_territories(self, border_territories: list):
        self.border_territories.extend(border_territories)

    def switch_placer(self, player: Player, soldier_quantity: int):
        self.player = player
        self.image.change_color(player.color)
        self.solider_number = soldier_quantity

    def remove_soldier(self, quantity: int):
        self.solider_number -= quantity

    def no_solider_remaining(self) -> bool:
        if self.solider_number < 1:
            return True
        return False

    def add_soldier(self, quantity: int):
        self.solider_number += quantity

    def select(self):
        self.image.change_color(Color.GREY)

    def unselect(self):
        self.image.change_color(self.player.color)

    def get_name(self) -> str:
        return self.name

    def get_image(self):
        return self.image.get_image()

    def get_center_position(self):
        return self.image.get_center()
