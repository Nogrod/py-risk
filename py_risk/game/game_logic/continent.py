class Continent:

    def __init__(self, name: str, holding_bonus: int, territories: list):
        self.name = name
        self.holding_bonus = holding_bonus
        self.territories = territories

    def get_player_holding_bonus(self, player) -> int:
        holding_player = self.territories[0].player
        for territory in self.territories[1:]:
            if territory.player != holding_player:
                holding_player = None
                break

        if holding_player == player:
            return self.holding_bonus
        else:
            return 0
