from py_risk import localization
from py_risk.game_asssets import Color, PlayerColorNameMapping


class Player:
    def __init__(self, color: Color):
        self.color = color
        self.alive = True
        self.name = self._get_name_from_color(color)

    @staticmethod
    def _get_name_from_color(color: Color):
        raw_name = PlayerColorNameMapping[color]
        name = localization.get_localization(raw_name)
        return name

    def get_name(self):
        return self.name
