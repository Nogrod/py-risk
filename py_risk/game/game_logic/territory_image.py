import pygame


class TerritoryImage:

    def __init__(self, image: pygame.Surface):
        self.image = image
        self.bounding_rect = image.get_bounding_rect()
        self.center = self.bounding_rect.center

    def get_image(self) -> pygame.Surface:
        return self.image

    def get_center(self):
        return self.center

    def change_color(self, color: pygame.Color):
        new_image = self.image.copy()
        for x in range(0, self.bounding_rect.width):
            for y in range(0, self.bounding_rect.height):
                if self.image.get_at((self.bounding_rect.x + x, self.bounding_rect.y + y)) != (0, 0, 0):
                    new_image.set_at((self.bounding_rect.x + x, self.bounding_rect.y + y), color)
        self.image = new_image
