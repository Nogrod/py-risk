from py_risk.game.game_logic.game_map import GameMap
from py_risk.game.game_logic.player import Player


class PlayerLogic:

    def __init__(self, players: list, game_map: GameMap):
        self.game_map = game_map
        self.players = players
        self.winning_player = None

    def update_dead_player(self):
        alive_player = self.game_map.get_player_who_hold_territory()
        self.players = [player for player in self.players if player in alive_player]

    def check_winning_condition(self) -> bool:
        if len(self.players) == 1:
            self.winning_player = self.players[0]
            return True
        return False

    def get_winning_player(self) -> Player:
        return self.winning_player
