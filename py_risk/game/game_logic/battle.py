import random
from enum import Enum

from py_risk.game.game_logic.player import Player
from py_risk.game.game_logic.territory import Territory


class BattleOutcome(Enum):
    ATTACKER_WIN = 0
    DEFENDER_WIN = 1


class Battle:
    def __init__(self, source_territory: Territory, target_territory: Territory, attacking_player: Player,
                 attacking_solider: int,
                 defender_solider: int):
        self.source_territory = source_territory
        self.target_territory = target_territory
        self.attacking_solider = attacking_solider
        self.defender_solider = defender_solider
        self.attacking_player = attacking_player

    def run(self):
        attacker_dices = self._roll_dices(self.attacking_solider)
        defender_dices = self._roll_dices(self.defender_solider)

        dead_attacker, dead_defender = self._calculate_deaths(attacker_dices, defender_dices)
        self._remove_dead_solider(dead_attacker, dead_defender)
        battle_outcome = self._evaluate_battle()
        if battle_outcome == BattleOutcome.ATTACKER_WIN:
            self._switch_territory(dead_attacker)
        return battle_outcome

    @staticmethod
    def _calculate_deaths(attacker_dices: list, defender_dices: list) -> tuple:
        # get sorted attacker and defender dices

        dead_attacker = 0
        dead_defender = 0
        for attacker_dice, defender_dice in zip(attacker_dices, defender_dices):
            if attacker_dice > defender_dice:
                dead_defender += 1
            else:
                dead_attacker += 1

        return dead_attacker, dead_defender

    @staticmethod
    def _roll_dices(quantity: int) -> list:
        dices = [random.randint(1, 6) for _ in range(quantity)]
        dices.sort(reverse=True)
        return dices

    def _remove_dead_solider(self, dead_attacker: int, dead_defender: int):
        self.target_territory.remove_soldier(dead_defender)
        self.source_territory.remove_soldier(dead_attacker)

    def _switch_territory(self, dead_attacker):
        survived_attacker = self.attacking_solider - dead_attacker
        self.target_territory.switch_placer(self.attacking_player, survived_attacker)
        self.source_territory.remove_soldier(self.attacking_solider)

    def _evaluate_battle(self) -> BattleOutcome:
        if self.target_territory.no_solider_remaining():
            battle_outcome = BattleOutcome.ATTACKER_WIN
        else:
            battle_outcome = BattleOutcome.DEFENDER_WIN

        return battle_outcome
