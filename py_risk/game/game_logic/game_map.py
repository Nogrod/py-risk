from __future__ import annotations

from py_risk.game.game_logic.player import Player
from py_risk.game.game_logic.territory import Territory


class GameMap:

    def __init__(self, territories: dict, continents: dict):
        self.continents = continents
        self.territories = territories

    @staticmethod
    def check_player_territory_connection(source_territory: Territory, target_territory: Territory) -> bool:
        todo_nodes = [source_territory]
        done_nodes = []
        while todo_nodes:
            working_node = todo_nodes.pop(0)
            done_nodes.append(working_node)

            for connected_territory in working_node.border_territories:
                if source_territory.player != connected_territory.player:
                    continue
                if connected_territory == target_territory:
                    return True
                if connected_territory not in done_nodes:
                    todo_nodes.append(connected_territory)
        return False

    def get_territory(self, territory_id: int) -> Territory:
        return self.territories.get(territory_id)

    def get_player_territory_quantity(self, player: Player) -> int:
        quantity = 0
        for territory in self.territories.values():
            if territory.player == player:
                quantity += 1
        return quantity

    def get_player_continent_bonus(self, player: Player) -> int:
        continent_bonus = 0
        for continent in self.continents:
            continent_bonus += continent.get_player_holding_bonus(player)
        return continent_bonus

    def get_player_who_hold_territory(self) -> set:
        players = set()
        for territory in self.territories.values():
            players.add(territory.player)

        return players
