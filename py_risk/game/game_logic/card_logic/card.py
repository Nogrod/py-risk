from __future__ import annotations

from enum import Enum

from py_risk.game.game_logic.territory import Territory


class CardType(Enum):
    INFANTRY = 0
    CAVALRY = 1
    ARTILLERY = 2
    JOKER = 3


class CardSoliderBonus(Enum):
    INFANTRY = 4
    CAVALRY = 6
    ARTILLERY = 8
    DIFFERENT = 10
    NONE = 0


class CardSelectState(Enum):
    SELECTED = 0
    UNSELECTED = 1


class Card:

    def __init__(self, territory: Territory, card_type: CardType):
        self.territory = territory
        self.type_ = card_type

    def equal_type(self, other: Card) -> bool:
        if other.type_ == CardType.JOKER or self.type_ == CardType.JOKER:
            return True
        if self.type_ == other.type_:
            return True
        return False

    def unequal_type(self, other: Card) -> bool:
        return not self.equal_type(other)

    def get_territory_name(self) -> str:
        if self.territory is not None:
            return self.territory.get_name()
        else:
            return ""

    def get_card_type_name(self) -> str:
        return self.type_.name
