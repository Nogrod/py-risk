import abc

from py_risk.game.game_logic.card_logic.card import Card, CardSelectState, CardType


class CardDeckInterface(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def draw_card(self):
        pass

    @abc.abstractmethod
    def return_cards(self, cards: list):
        pass

    @abc.abstractmethod
    def _deck_empty_action(self):
        pass

    @abc.abstractmethod
    def _shuffle_card_deck(self):
        pass


class CardSetInterface(metaclass=abc.ABCMeta):
    def __init__(self):
        self.selected_cards = []
        self.set_troop_value = 0

    def __contains__(self, item: Card) -> bool:
        result = item in self.selected_cards
        return result

    @abc.abstractmethod
    def _update_set_troop_value(self):
        pass

    @abc.abstractmethod
    def pop_all_cards(self):
        pass

    def toggle_card(self, card: Card) -> CardSelectState:
        if card in self.selected_cards:
            self._unselect_card(card)
            return CardSelectState.UNSELECTED
        else:
            self._select_card(card)
            return CardSelectState.SELECTED

    def _select_card(self, card: Card):
        invalid_action = False
        if len(self.selected_cards) >= 3:
            invalid_action = True
        elif card.type_ == CardType.JOKER:
            selected_card_types = [card.type_ for card in self.selected_cards]
            if CardType.JOKER in selected_card_types:
                invalid_action = True  # can't select two joker at the same time
        elif card in self.selected_cards:
            invalid_action = True

        if invalid_action:
            return

        self.selected_cards.append(card)
        self._update_set_troop_value()

    def _unselect_card(self, card: Card):
        card_index = self.selected_cards.index(card)
        self.selected_cards.pop(card_index)
        self._update_set_troop_value()

    def can_activate_cards(self) -> bool:
        if self.set_troop_value == 0:
            return False
        else:
            return True

    def get_card_set_value(self):
        return self.set_troop_value

    def _correct_number_of_selected_cards(self):
        if len(self.selected_cards) != 3:
            return False
        else:
            return True

    def _check_three_same_selected_cards(self) -> bool:
        result = (self.selected_cards[0].equal_type(self.selected_cards[1])
                  and self.selected_cards[1].equal_type(self.selected_cards[2]))
        return result

    def _check_three_different_selected_cards(self) -> bool:
        result = (self.selected_cards[0].unequal_type(self.selected_cards[1])
                  and self.selected_cards[0].unequal_type(self.selected_cards[2])
                  and self.selected_cards[1].unequal_type(self.selected_cards[2]))
        return result
