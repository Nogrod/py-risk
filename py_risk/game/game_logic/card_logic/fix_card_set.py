from py_risk.game.game_logic.card_logic.interfaces import CardSetInterface
from py_risk.game.game_logic.card_logic.card import CardType, CardSoliderBonus


class FixCardSet(CardSetInterface):

    def pop_all_cards(self):
        cards = self.selected_cards
        self.selected_cards = []
        return cards

    def _update_set_troop_value(self):
        if not self._correct_number_of_selected_cards():
            card_value = CardSoliderBonus.NONE
        elif self._check_three_different_selected_cards():
            card_value = CardSoliderBonus.DIFFERENT
        elif self._check_three_same_selected_cards():
            card_type = self._get_same_card_type()
            card_value = self._get_same_card_bonus(card_type)
        else:
            card_value = CardSoliderBonus.NONE
        self.set_troop_value = card_value.value

    @staticmethod
    def _get_same_card_bonus(given_card_type: CardType) -> CardSoliderBonus:
        for card_type in list(CardType):
            if card_type == given_card_type:
                card_bonus = CardSoliderBonus[card_type.name]
                return card_bonus

    def _get_same_card_type(self):
        selected_card_types = [card.type_ for card in self.selected_cards]
        if CardType.JOKER in selected_card_types:
            joker_index = selected_card_types.index(CardType.JOKER)
            none_joker_card_index = (joker_index + 1) % 3
            return self.selected_cards[none_joker_card_index].type_
        else:
            return self.selected_cards[0].type_
