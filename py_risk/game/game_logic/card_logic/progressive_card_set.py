from py_risk.game.game_logic.card_logic.interfaces import CardSetInterface


class CardRewardManager:

    def __init__(self):
        self.activation_counter = 1

    def get_current_reward(self) -> int:
        activation_formal_threshold = 6
        if self.activation_counter < activation_formal_threshold:
            return 2 + self.activation_counter * 2
        else:
            return 15 + 5 * (self.activation_counter - activation_formal_threshold)

    def activate_card_set(self):
        self.activation_counter += 1


class ProgressiveCardSet(CardSetInterface):

    def __init__(self, card_reward_manager: CardRewardManager):
        super().__init__()
        self.card_reward_manager = card_reward_manager

    def pop_all_cards(self):
        cards = self.selected_cards
        self.selected_cards = []
        self.card_reward_manager.activate_card_set()
        return cards

    def _update_set_troop_value(self):
        self.set_troop_value = self.card_reward_manager.get_current_reward()
