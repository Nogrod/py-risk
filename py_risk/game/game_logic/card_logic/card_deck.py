import random

from py_risk.game.game_logic.card_logic.interfaces import CardDeckInterface


class CardDeck(CardDeckInterface):

    def __init__(self, cards: list):
        self.card_deck = cards
        self._shuffle_card_deck()

        self.used_cards = []

    def draw_card(self):
        card = self.card_deck.pop(0)
        if len(self.card_deck) == 0:
            self._deck_empty_action()
        return card

    def return_cards(self, cards: list):
        self.used_cards.extend(cards)

    def _deck_empty_action(self):
        self._shuffle_card_deck()
        self.card_deck = self.used_cards
        self.used_cards = []

    def _shuffle_card_deck(self):
        random.shuffle(self.card_deck)
