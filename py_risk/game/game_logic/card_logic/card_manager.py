from py_risk.game.game_logic.card_logic.interfaces import CardDeckInterface, CardSetInterface
from py_risk.game.game_logic.card_logic.card import CardSelectState, Card
from py_risk.game.game_logic.player import Player


class CardManager:
    def __init__(self, card_deck: CardDeckInterface, card_set: CardSetInterface, player: Player):
        self.card_deck = card_deck
        self.draw_card_in_turn = False
        self.hand_cards = []
        self.card_set = card_set
        self.player = player

    def draw_card(self):
        if not self.draw_card_in_turn:
            card = self.card_deck.draw_card()
            self.hand_cards.append(card)
            self.draw_card_in_turn = False

    def select_card(self, card: Card) -> CardSelectState:
        return self.card_set.toggle_card(card)

    def get_cards(self) -> list:
        return self.hand_cards

    def activate_set(self) -> int:
        additional_solider = 0
        if self.card_set.can_activate_cards():
            additional_solider = self.card_set.get_card_set_value()
            activated_cards = self.card_set.pop_all_cards()
            self._activate_cards(activated_cards)
            self.card_deck.return_cards(activated_cards)
            self._remove_activated_hand_cards(activated_cards)

        return additional_solider

    def _remove_activated_hand_cards(self, cards: list):
        for card in cards:
            self.hand_cards.remove(card)

    def turn_end_actions(self):
        self.draw_card_in_turn = False

    def get_number_of_cards(self) -> int:
        return len(self.hand_cards)

    def is_card_selected(self, card: Card) -> bool:
        result = card in self.card_set
        return result

    def _activate_cards(self, cards: list):
        for card in cards:
            if card.territory is not None and card.territory.player == self.player:
                card.territory.add_soldier(2)

    def can_activate_set(self) -> bool:
        return self.card_set.can_activate_cards()
