import pygame

from py_risk.game.Event import TerritoryClickEvent, NextPhaseEvent, SoldierCountEvent, SelectCardEvent
from py_risk.game.game_logic.card_logic.card_manager import CardManager
from py_risk.game.game_logic.game_map import GameMap
from py_risk.game.game_logic.turn_logic.attack_phase import AttackPhase
from py_risk.game.game_logic.turn_logic.attack_phase_interface import AttackPhaseInterface
from py_risk.game.game_logic.turn_logic.movement_phase import MovementPhase
from py_risk.game.game_logic.player import Player
from py_risk.game.game_logic.territory import Territory
from py_risk.localization import get_localization


class AttackMovementPhase(MovementPhase, AttackPhaseInterface):
    def __init__(self, player: Player, game_map: GameMap, selected_territory: Territory,
                 conquered_territory: Territory, card_manager: CardManager):
        super().__init__(player, game_map, card_manager)
        self.selected_territory = selected_territory
        self.conquered_territory = conquered_territory

        # overwrite self.events from super class
        self.events = {
            TerritoryClickEvent: self.territory_click,
            NextPhaseEvent: self.change_to_movement_phase,
            SoldierCountEvent: self.solider_count_manager,
            SelectCardEvent: self.select_card,
        }

    def get_name(self) -> str:
        return get_localization("attack")

    def territory_click(self, event: TerritoryClickEvent):
        territory = self.game_map.get_territory(event.territory_id)
        if territory == self.conquered_territory:
            self.move_solider(territory, self.solider_counter)

        self.unselect_territory()
        self.select_territory(territory)
        return AttackPhase(self.player, self.game_map, self.selected_territory, self.card_manager)