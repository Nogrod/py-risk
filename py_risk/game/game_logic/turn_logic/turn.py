import pygame

from py_risk.game.game_logic.card_logic.card_manager import CardManager
from py_risk.game.game_logic.game_map import GameMap
from py_risk.game.game_logic.turn_logic.phase import Phase
from py_risk.game.game_logic.turn_logic.change_player_phase import ChangePlayerPhase
from py_risk.game.game_logic.turn_logic.reinforcement_phase import ReinforcementPhase
from py_risk.game.game_logic.turn_logic.troop_distribution_phase import TroopDistributionPhase
from py_risk.game.game_logic.turn_logic.end_init_phase import EndInitPhase


class Turn:

    def __init__(self, players: list, game_map: GameMap, card_managers: dict):
        self.players = players
        self.current_player = players[0]
        self.game_map = game_map
        self.card_managers = card_managers
        self.current_turn_phase: Phase = TroopDistributionPhase(self.current_player, self.game_map, self.players, card_managers[self.current_player])

    def event_handler(self, event: pygame.event.Event):
        if event.type == pygame.USEREVENT:
            self.current_turn_phase = self.current_turn_phase.event_handler(event)

            if type(self.current_turn_phase) is ChangePlayerPhase:
                self._next_turn()

            if type(self.current_turn_phase) is EndInitPhase:
                self._end_init_phase()

    def _end_init_phase(self):
        self.current_turn_phase = ReinforcementPhase(self.players[0], self.game_map, self.card_managers[self.current_player])

    def _next_turn(self):
        self._set_next_player()
        self.current_turn_phase = ReinforcementPhase(self.current_player, self.game_map, self.card_managers[self.current_player])

    def _set_next_player(self):
        current_player_index = self.players.index(self.current_turn_phase.player)
        next_player_index = (current_player_index + 1) % len(self.players)
        next_player = self.players[next_player_index]
        self.current_player = next_player

    def get_solider_number(self) -> int:
        return self.current_turn_phase.solider_counter

    def get_reinforcement_quantity(self) -> int:
        if isinstance(self.current_turn_phase, ReinforcementPhase):
            return self.current_turn_phase.reinforcement_quantity
        else:
            return 0

    def get_current_player_name(self):
        return self.current_turn_phase.player.name

    def get_current_card_manager(self) -> CardManager:
        return self.current_turn_phase.card_manager

    def can_activate_hand_cards(self) -> bool:
        return type(self.current_turn_phase) is ReinforcementPhase

    def get_current_phase_name(self) -> str:
        return self.current_turn_phase.get_name()
