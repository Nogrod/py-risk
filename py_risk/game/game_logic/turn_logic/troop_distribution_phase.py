from py_risk.game.game_logic.card_logic.card_manager import CardManager
from py_risk.game.game_logic.game_map import GameMap
from py_risk.game.game_logic.turn_logic.end_init_phase import EndInitPhase
from py_risk.game.game_logic.turn_logic.phase import Phase
from py_risk.game.game_logic.turn_logic.reinforcement_phase import ReinforcementPhase
from py_risk.game.game_logic.player import Player


class TroopDistributionPhase(ReinforcementPhase):

    def __init__(self, player: Player, game_map: GameMap, players: list, card_manager: CardManager):
        self.players = players
        super().__init__(player, game_map, card_manager)

    def calculate_reinforcement_quantity(self) -> int:
        max_quantity = 40
        quantity_loss = 5
        min_player = 2
        quantity_loss_factor = len(self.players) - min_player
        already_reinforced = self.game_map.get_player_territory_quantity(self.player)

        reinforcement_quantity = max_quantity - quantity_loss * quantity_loss_factor - already_reinforced
        return reinforcement_quantity

    def phase_end_handler(self) -> Phase:
        return self.check_last_player()

    def check_last_player(self):
        player_index = self.players.index(self.player)
        if player_index + 1 == len(self.players):
            return EndInitPhase(self.player)
        else:
            next_player = self.players[player_index + 1]
            return TroopDistributionPhase(next_player, self.game_map, self.players, self.card_manager)
