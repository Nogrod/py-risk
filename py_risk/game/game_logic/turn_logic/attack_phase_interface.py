import abc

from py_risk.game.game_logic.turn_logic.movement_phase import MovementPhase
from py_risk.game.game_logic.turn_logic.phase import Phase


class AttackPhaseInterface(Phase, metaclass=abc.ABCMeta):

    def change_to_movement_phase(self, event=None) -> Phase:
        self.unselect_territory()
        return MovementPhase(self.player, self.game_map, self.card_manager)