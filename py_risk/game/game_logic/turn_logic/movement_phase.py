import pygame

from py_risk.game.Event import get_risk_event, TerritoryClickEvent, SoldierCountEvent, NextPhaseEvent, SelectCardEvent, \
    RiskEvent
from py_risk.game.game_logic.card_logic.card_manager import CardManager
from py_risk.game.game_logic.game_map import GameMap
from py_risk.game.game_logic.player import Player
from py_risk.game.game_logic.turn_logic.phase import Phase
from py_risk.game.game_logic.turn_logic.change_player_phase import ChangePlayerPhase
from py_risk.game.game_logic.territory import Territory
from py_risk.localization import get_localization


class MovementPhase(Phase):

    def __init__(self, player: Player, game_map: GameMap, card_manager: CardManager):
        events = {
            TerritoryClickEvent: self.territory_click,
            SoldierCountEvent: self.solider_count_manager,
            SelectCardEvent: self.select_card,
            NextPhaseEvent: self.next_phase,
        }

        super().__init__(player, game_map, card_manager, events)

    def next_phase(self, event: RiskEvent):
        self.unselect_territory()
        return self.end_turn()

    def territory_click(self, event: TerritoryClickEvent):
        territory = self.game_map.get_territory(event.territory_id)
        if self.selected_territory is not None:
            self.move_solider(territory, self.solider_counter)
            self.unselect_territory()
            return self.end_turn()
        else:
            self.select_territory(territory)

    def get_name(self) -> str:
        return get_localization("movement")

    def end_turn(self):
        self.card_manager.turn_end_actions()
        return ChangePlayerPhase(self.player)

    def specific_solider_count_rules(self):
        if self.selected_territory is None:
            return

        solider_threshold = self.selected_territory.solider_number - 1
        if self.solider_counter > solider_threshold:
            self.solider_counter = solider_threshold

    def move_solider(self, target_territory: Territory, quantity: int):
        if self.check_troop_movement(self.selected_territory, target_territory, quantity):
            self.selected_territory.remove_soldier(quantity)
            target_territory.add_soldier(quantity)
        else:
            print("Invalid action")

    def check_troop_movement(self, source_territory: Territory, target_territory: Territory, quantity: int) -> bool:
        if source_territory.solider_number - quantity <= 0:
            return False
        if source_territory.player != target_territory.player:
            return False
        if not self.game_map.check_player_territory_connection(source_territory, target_territory):
            return False
        return True
