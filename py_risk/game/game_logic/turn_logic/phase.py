from __future__ import annotations

import abc

import pygame

from py_risk.game.Event import SoldierCountEvent, SelectCardEvent, get_risk_event
from py_risk.game.game_logic.card_logic.card_manager import CardManager
from py_risk.game.game_logic.game_map import GameMap
from py_risk.game.game_logic.turn_logic.phase_interfaces import PhaseInterface
from py_risk.game.game_logic.player import Player
from py_risk.game.game_logic.territory import Territory


class Phase(PhaseInterface, metaclass=abc.ABCMeta):
    selected_territory: Territory

    def __init__(self, player: Player, game_map: GameMap, card_manager: CardManager, events: dict):
        self.player = player
        self.game_map = game_map
        self.selected_territory = None
        self.solider_counter = 2
        self.card_manager = card_manager
        self.events = events

    def event_handler(self, event: pygame.event.Event) -> Phase:
        event = get_risk_event(event)
        if event is not None:
            event_result = self.events[event.__class__](event)
            if event_result is not None:
                return event_result

        self.specific_solider_count_rules()
        return self

    @abc.abstractmethod
    def get_name(self) -> str:
        raise NotImplemented

    @abc.abstractmethod
    def specific_solider_count_rules(self) -> str:
        raise NotImplemented

    def solider_count_manager(self, event: SoldierCountEvent):
        if event.solider_counter_direction > 0:
            self.solider_counter += 1
        elif event.solider_counter_direction < 0:
            self.solider_counter -= 1

        if self.solider_counter < 0:
            self.solider_counter = 0

    def unselect_territory(self):
        if self.selected_territory is not None:
            self.selected_territory.unselect()
            self.selected_territory = None

    def select_territory(self, territory: Territory):
        if self.player == territory.player:
            self.selected_territory = territory
            self.selected_territory.select()

    def select_card(self, event: SelectCardEvent):
        card = event.card
        self.card_manager.select_card(card)


