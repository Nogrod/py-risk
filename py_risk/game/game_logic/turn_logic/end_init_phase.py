from py_risk.game.game_logic.turn_logic.end_phase_interface import EndPhase
from py_risk.game.game_logic.player import Player


class EndInitPhase(EndPhase):
    
    def __init__(self, player: Player):
        self.player = player
