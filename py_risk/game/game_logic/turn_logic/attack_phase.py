from __future__ import annotations

import pygame

from py_risk.game.Event import get_risk_event, TerritoryClickEvent, NextPhaseEvent, SelectCardEvent, SoldierCountEvent
from py_risk.game.game_logic.battle import BattleOutcome, Battle
from py_risk.game.game_logic.card_logic.card_manager import CardManager
from py_risk.game.game_logic.game_map import GameMap
from py_risk.game.game_logic.turn_logic.attack_phase_interface import AttackPhaseInterface
from py_risk.game.game_logic.player import Player
from py_risk.game.game_logic.territory import Territory
from py_risk.localization import get_localization


class AttackPhase(AttackPhaseInterface):

    def __init__(self, player: Player, game_map: GameMap, selected_territory: Territory, card_manager: CardManager):
        events = {
            TerritoryClickEvent: self.territory_click,
            NextPhaseEvent: self.change_to_movement_phase,
            SoldierCountEvent: self.solider_count_manager,
            SelectCardEvent: self.select_card,
        }

        super().__init__(player, game_map, card_manager, events)
        self.selected_territory = selected_territory

    def get_name(self) -> str:
        return get_localization("attack")

    def territory_click(self, event: TerritoryClickEvent):
        territory = self.game_map.get_territory(event.territory_id)
        if self.selected_territory is not None:
            if self.check_attack(self.selected_territory, territory):
                self.specific_solider_count_rules()
                return self.attack_territory(territory)
            else:
                self.unselect_territory()
                self.select_territory(territory)
        else:
            self.select_territory(territory)

    def attack_territory(self, target_territory: Territory) -> AttackPhaseInterface:
        from py_risk.game.game_logic.turn_logic.attack_movement_phase import AttackMovementPhase  # need a local import to privent a circular import

        battle = Battle(self.selected_territory, target_territory, self.player, self.solider_counter,
                        self.get_defender_solider_quantity(target_territory))
        battle_outcome = battle.run()

        if battle_outcome == BattleOutcome.ATTACKER_WIN:
            self.card_manager.draw_card()
            return AttackMovementPhase(self.player, self.game_map, self.selected_territory, target_territory,
                                       self.card_manager)
        elif battle_outcome == BattleOutcome.DEFENDER_WIN:
            return self

    @staticmethod
    def check_attack(source_territory: Territory, target_territory: Territory) -> bool:
        if source_territory.player == target_territory.player:
            return False
        if target_territory not in source_territory.border_territories:
            return False
        if source_territory.solider_number < 2:
            return False
        return True

    def get_defender_solider_quantity(self, territory: Territory) -> int:
        quantity = territory.solider_number
        if quantity > 2:
            quantity = 2
        return quantity

    def specific_solider_count_rules(self):
        if self.selected_territory is None:
            return

        solider_threshold = self.selected_territory.solider_number - 1
        if self.solider_counter > solider_threshold:
            self.solider_counter = solider_threshold

        if self.solider_counter > 3:
            self.solider_counter = 3

        if self.solider_counter < 1:
            self.solider_counter = 1


