import abc

from py_risk.game.game_logic.turn_logic.phase_interfaces import PhaseInterface
from py_risk.game.game_logic.player import Player


class EndPhase(PhaseInterface, metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def __init__(self, player: Player):
        raise NotImplemented
