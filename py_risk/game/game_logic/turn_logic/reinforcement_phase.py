import pygame

from py_risk.game.Event import get_risk_event, TerritoryClickEvent, SoldierCountEvent, SelectCardEvent, \
    ActivateCardsEvent, RiskEvent
from py_risk.game.game_logic.card_logic.card_manager import CardManager
from py_risk.game.game_logic.game_map import GameMap
from py_risk.game.game_logic.turn_logic.attack_phase import AttackPhase
from py_risk.game.game_logic.turn_logic.phase import Phase
from py_risk.game.game_logic.player import Player
from py_risk.game.game_logic.territory import Territory
from py_risk.localization import get_localization


class ReinforcementPhase(Phase):

    def __init__(self, player: Player, game_map: GameMap, card_manager: CardManager):
        events = {
            TerritoryClickEvent: self.territory_click,
            SoldierCountEvent: self.solider_count_manager,
            SelectCardEvent: self.select_card,
            ActivateCardsEvent: self.activate_hand_cards,
        }

        super().__init__(player, game_map, card_manager, events)
        self.reinforcement_quantity = self.calculate_reinforcement_quantity()

    def get_name(self) -> str:
        return get_localization("reinforcement")

    def event_handler(self, event: pygame.event.Event):
        event = get_risk_event(event)
        if event is not None:
            event_result = self.events[event.__class__](event)
            if event_result is not None:
                return event_result

            self.specific_solider_count_rules()

        if self.check_phase_end():
            return self.phase_end_handler()
        else:
            return self

    def territory_click(self, event: TerritoryClickEvent):
        territory = self.game_map.get_territory(event.territory_id)
        self.reinforce_territory(territory)

    def phase_end_handler(self) -> Phase:
        return AttackPhase(self.player, self.game_map, None, self.card_manager)

    def check_phase_end(self):
        if self.reinforcement_quantity <= 0 and self.card_manager.get_number_of_cards() <= 4:
            return True
        return False

    def reinforce_territory(self, territory: Territory):
        if territory.player == self.player:
            territory.add_soldier(self.solider_counter)
            self.reinforcement_quantity -= self.solider_counter

    def calculate_reinforcement_quantity(self) -> int:
        territory_quantity = self.game_map.get_player_territory_quantity(self.player)
        territory_reinforcements = int(territory_quantity / 3)

        continent_reinforcements = self.game_map.get_player_continent_bonus(self.player)

        total_reinforcements = territory_reinforcements + continent_reinforcements
        if total_reinforcements < 3:
            total_reinforcements = 3

        return total_reinforcements

    def specific_solider_count_rules(self):
        solider_threshold = self.reinforcement_quantity
        if self.solider_counter > solider_threshold:
            self.solider_counter = solider_threshold

    def activate_hand_cards(self, event: RiskEvent):
        additional_reinforcement_solider = self.card_manager.activate_set()
        self.reinforcement_quantity += additional_reinforcement_solider


