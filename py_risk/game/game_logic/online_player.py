from enum import Enum

from py_risk.game.game_logic.player import Player
from py_risk.game_asssets import Color


class OnlinePlayerType(Enum):
    local = 0
    remote = 1


class OnlinePlayer(Player):
    def __init__(self, color: Color, local: bool, ip_address: str):
        super().__init__(color)
        self.local = local
        self.ip_address = ip_address
