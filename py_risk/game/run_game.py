import os

import pygame

from py_risk.builder import build_game
from py_risk.game_asssets import RiskEventType
from py_risk.game_info import GameInformation


def run(game_info: GameInformation):
    window_surface = game_info.window_surface
    ui_manager = game_info.ui_manager

    game = build_game.build(game_info, ui_manager)

    clock = pygame.time.Clock()
    while True:
        time_delta = clock.tick(60) / 1000.0
        game_events, ui_events = get_events()

        game.process_events(ui_events)

        game.update(game_events)

        game.render(window_surface, time_delta)


def get_events() -> tuple:
    game_events = []
    ui_events = []
    for event in pygame.event.get():
        if event.type == pygame.USEREVENT and event.user_type == RiskEventType.PY_RISK:
            game_events.append(event)
        else:
            ui_events.append(event)

    return game_events, ui_events
