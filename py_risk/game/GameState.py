import pygame

from py_risk.game.game_logic.player_logic import PlayerLogic
from py_risk.game.game_logic.turn_logic.turn import Turn
from py_risk.game.view.ui import UI
from py_risk.helper import exit_game


class GameState:

    def __init__(self, ui: UI, turn: Turn, player_logic: PlayerLogic, ui_manager):
        self.player_logic = player_logic
        self.turn = turn
        self.ui = ui
        self.ui_manager = ui_manager

    def update(self, game_events: list):
        for event in game_events:
            self.turn.event_handler(event)

        self.player_logic.update_dead_player()
        if self.player_logic.check_winning_condition():
            winning_player = self.player_logic.get_winning_player()
            self.ui.end_game(winning_player)

    def process_events(self, events: list):
        for event in events:
            if event.type == pygame.QUIT:
                exit_game()
            self.ui_manager.process_events(event)
            self.ui.handle_input(event)

    def render(self, window_surface: pygame.Surface, time_delta: float):
        self.ui.draw(window_surface)
        self.ui_manager.update(time_delta)
        self.ui_manager.draw_ui(window_surface)

        pygame.display.update()
