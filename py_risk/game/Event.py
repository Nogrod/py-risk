from __future__ import annotations

import abc

import pygame

from py_risk.game.game_logic.card_logic.card import Card
from py_risk.game_asssets import RiskEventType


class RiskEvent(metaclass=abc.ABCMeta):

    def __init__(self, risk_event_type: RiskEventType, **kwargs):
        self.type_ = risk_event_type
        self.event = self._create_event(**kwargs)

    def post(self):
        pygame.event.post(self.event)

    def _create_event(self, **kwargs) -> pygame.event.Event:
        event_data = {"user_type": RiskEventType.PY_RISK, "risk_type": self.type_}
        event_data.update(kwargs)
        event = pygame.event.Event(pygame.USEREVENT, event_data)
        return event

    @staticmethod
    @abc.abstractmethod
    def check_event(event: pygame.event.Event) -> bool:
        pass

    @staticmethod
    def generic_check_event(event: pygame.event.Event, event_type: RiskEventType) -> bool:
        if event.user_type == RiskEventType.PY_RISK and event.risk_type == event_type:
            return True
        else:
            return False

    @staticmethod
    @abc.abstractmethod
    def parse_risk_event(event: pygame.event.Event) -> RiskEvent:
        pass


class SelectCardEvent(RiskEvent):

    def __init__(self, card: Card):
        super().__init__(RiskEventType.SELECT_CARD, card=card)
        self.card = card

    @staticmethod
    def check_event(event: pygame.event.Event) -> bool:
        return RiskEvent.generic_check_event(event, RiskEventType.SELECT_CARD)

    @staticmethod
    def parse_risk_event(event: pygame.event.Event) -> RiskEvent:
        return SelectCardEvent(event.card)


class ActivateCardsEvent(RiskEvent):
    def __init__(self):
        super().__init__(RiskEventType.ACTIVATE_CARDS)

    @staticmethod
    def check_event(event: pygame.event.Event) -> bool:
        return RiskEvent.generic_check_event(event, RiskEventType.ACTIVATE_CARDS)

    @staticmethod
    def parse_risk_event(event: pygame.event.Event) -> RiskEvent:
        return ActivateCardsEvent()


class SoldierCountEvent(RiskEvent):
    def __init__(self, solider_counter_direction: int):
        super().__init__(RiskEventType.SOLIDER_COUNTER, solider_counter_direction=solider_counter_direction)
        self.solider_counter_direction = solider_counter_direction

    @staticmethod
    def check_event(event: pygame.event.Event) -> bool:
        return RiskEvent.generic_check_event(event, RiskEventType.SOLIDER_COUNTER)

    @staticmethod
    def parse_risk_event(event: pygame.event.Event) -> RiskEvent:
        return SoldierCountEvent(event.solider_counter_direction)


class NextPhaseEvent(RiskEvent):

    def __init__(self):
        super().__init__(RiskEventType.NEXT_PHASE)

    @staticmethod
    def parse_risk_event(event: pygame.event.Event) -> RiskEvent:
        return NextPhaseEvent()

    @staticmethod
    def check_event(event: pygame.event.Event) -> bool:
        return RiskEvent.generic_check_event(event, RiskEventType.NEXT_PHASE)


class TerritoryClickEvent(RiskEvent):

    def __init__(self, territory_id: int):
        super().__init__(RiskEventType.TERRITORY_PRESSED, territory_id=territory_id)
        self.territory_id = territory_id

    @staticmethod
    def parse_risk_event(event: pygame.event.Event) -> RiskEvent:
        return TerritoryClickEvent(event.territory_id)

    @staticmethod
    def check_event(event: pygame.event.Event) -> bool:
        return RiskEvent.generic_check_event(event, RiskEventType.TERRITORY_PRESSED)


def get_risk_event(event: pygame.event.Event) -> RiskEvent:

    if event.user_type == RiskEventType.PY_RISK:
        type_class_mapping = {
            RiskEventType.NEXT_PHASE: NextPhaseEvent,
            RiskEventType.TERRITORY_PRESSED: TerritoryClickEvent,
            RiskEventType.SELECT_CARD: SelectCardEvent,
            RiskEventType.ACTIVATE_CARDS: ActivateCardsEvent,
            RiskEventType.SOLIDER_COUNTER: SoldierCountEvent,
        }
        event_class = type_class_mapping.get(event.risk_type, None)
        if event_class is not None:
            risk_event = event_class.parse_risk_event(event)
            return risk_event
        else:
            return None
