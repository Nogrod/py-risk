import os
from enum import Enum

import pygame
import pygame_gui
import yaml

from py_risk.localization import SupportedLanguages


class GameInformation:
    def __init__(self):
        self.window_size = (1300, 900)
        self.map_size = (self.window_size[0], int(8 * self.window_size[1] / 10))
        self.player = []
        self.window_surface = pygame.display.set_mode(size=self.window_size)
        self.ui_manager = pygame_gui.UIManager(window_resolution=self.window_size, theme_path="themes.json")
        self.singe_player = True
        self.card_bonus_type = CardBonusType.FIX

        self.language, self.card_bonus_type = self.load_settings()

    def load_settings(self):
        settings_path = os.path.join("..", "settings.yaml")

        with open(settings_path, "r", encoding='UTF-8') as stream:
            settings = yaml.safe_load(stream)
        language = self.parse_language_settings(settings["language"])
        reward_type = self.parse_card_reward_settings(settings["card reward game mode"])
        return language, reward_type

    @staticmethod
    def parse_language_settings(language: str):
        language = language.lower()
        if language == "german":
            return SupportedLanguages.GERMAN
        elif language == "english":
            return SupportedLanguages.ENGLISH
        else:
            raise ValueError("Invalid Valid for Language")

    @staticmethod
    def parse_card_reward_settings(type_: str):
        type_ = type_.lower()
        if type_ == "fixed":
            return CardBonusType.FIX
        elif type_ == "progressive":
            return CardBonusType.PROGRESSIVE
        else:
            raise ValueError("Invalid Valid for Language")


class CardBonusType(Enum):
    FIX = 0
    PROGRESSIVE = 1
