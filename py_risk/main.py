import pygame

from py_risk.game_info import GameInformation
from py_risk.game import run_game
from py_risk.before_game import run_main_menu
from py_risk.localization import init_localization


def main():
    pygame.init()
    pygame.display.set_caption("Py-risk")

    game_info = GameInformation()

    init_localization(game_info.language)

    run_main_menu.run(game_info)
    run_game.run(game_info)


if __name__ == '__main__':
    main()
