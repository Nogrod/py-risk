import pygame
from pygame_gui.core.interfaces import IUIManagerInterface

from py_risk.game.game_logic.turn_logic.turn import Turn
from py_risk.game.view.user_bar import InformationPanel, TurnPanel, UserBar


class UserBarBuilder:

    def __init__(self, map_size: tuple, window_size: tuple, ui_manager: IUIManagerInterface, turn: Turn):
        pos = (0, map_size[1])
        size = (window_size[0] * .2, window_size[1] - map_size[1])
        information_panel = InformationPanel(ui_manager, turn, pygame.Rect(pos, size))

        pos = (window_size[0] * 0.7, map_size[1])
        size = (window_size[0] * .3, window_size[1] - map_size[1])
        turn_panel = TurnPanel(ui_manager, turn, pygame.Rect(pos, size))

        self.user_bar = UserBar(ui_manager, information_panel, turn_panel)

    def build_user_bar(self):
        return self.user_bar