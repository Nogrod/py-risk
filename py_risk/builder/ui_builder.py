import os

from pygame_gui.core.interfaces import IUIManagerInterface

from py_risk.game.game_logic.turn_logic.turn import Turn
from py_risk.game.view.GameEndUi import GameEndUi
from py_risk.game.view.game_map_gui import GameMapGUI
from py_risk.game.view.game_ui import GameUi
from py_risk.game.view.hand_ui import HandCardUIHandler
from py_risk.game.view.menu_handler import MenuHandler
from py_risk.game.view.ui import UI
from py_risk.game.view.user_bar import UserBar
from py_risk.helper import load_image


class UIBuilder:
    def __init__(self, ui_manager: IUIManagerInterface, turn: Turn, game_map_gui: GameMapGUI, user_bar: UserBar):
        self.game_ui = None
        game_ui = self._build_game_ui(ui_manager, turn, game_map_gui, user_bar)
        menu_handler = self._build_menu_handler(ui_manager)
        hand_card_ui = self._build_hand_card_ui(ui_manager, turn, game_ui, game_map_gui)
        game_end_ui = self._build_gane_end_ui(ui_manager)

        background = load_image(os.path.join("data", "background.jpg"), game_map_gui.map_rect.size)
        self.ui = UI(game_ui, menu_handler, hand_card_ui, game_end_ui, background)

    def get_ui(self) -> UI:
        return self.ui

    def _build_gane_end_ui(self, ui_manager: IUIManagerInterface):
        gamed_end_ui = GameEndUi(ui_manager)
        return gamed_end_ui

    def _build_game_ui(self, ui_manager: IUIManagerInterface, turn: Turn, game_map_gui: GameMapGUI, user_bar: UserBar):
        game_ui = GameUi(ui_manager, turn, game_map_gui, user_bar)
        return game_ui

    def _build_menu_handler(self,  ui_manager: IUIManagerInterface):
        world_ui = MenuHandler(ui_manager)
        return world_ui

    def _build_hand_card_ui(self, ui_manager: IUIManagerInterface, turn: Turn, game_ui: GameUi, game_map_gui: GameMapGUI):
        hand_card_ui = HandCardUIHandler(turn, ui_manager, game_ui.user_bar.information_panel.show_cards_button, game_map_gui.map_rect)
        return hand_card_ui