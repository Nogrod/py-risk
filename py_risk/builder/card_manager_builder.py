import os

import yaml

from py_risk.game.game_logic.card_logic.card_deck import CardDeck
from py_risk.game.game_logic.card_logic.card_manager import CardManager
from py_risk.game.game_logic.card_logic.fix_card_set import FixCardSet
from py_risk.game.game_logic.card_logic.progressive_card_set import ProgressiveCardSet, CardRewardManager
from py_risk.game.game_logic.card_logic.card import Card, CardType
from py_risk.game_info import GameInformation, CardBonusType


class CardManagerBuilder:

    def __init__(self, territories: dict, players: list, game_information: GameInformation):
        cards = self._build_cards(territories)
        if game_information.singe_player:
            card_deck = self._build_card_deck(cards)
        else:
            card_deck = self._build_card_deck(cards)  # todo hier würde ein Multiplayerkartendeck, das sicher vor manipulation ist

        if game_information.card_bonus_type == CardBonusType.FIX:
            card_sets = self._build_fix_card_sets(players)
        elif game_information.card_bonus_type == CardBonusType.PROGRESSIVE:
            card_sets = self._build_progressive_card_sets(players)

        self.card_managers = self._build_card_managers(players, card_deck, card_sets)

    def get_card_managers(self) -> dict:
        return self.card_managers

    @staticmethod
    def _build_cards(territories: dict):
        with open(os.path.join("data", "game_data", "card_data.yaml"), "r") as stream:
            card_data = yaml.safe_load(stream)
            card_data = card_data.get("cards")

        joker_card_1 = Card(None, CardType.JOKER)
        joker_card_2 = Card(None, CardType.JOKER)
        cards = [joker_card_1, joker_card_2]
        for territory in territories.values():
            card_type = CardManagerBuilder._parse_card_type(card_data[territory.id_])
            cards.append(Card(territory, card_type))

        return cards

    @staticmethod
    def _parse_card_type(type_: str) -> CardType:
        if type_ == "inf":
            return CardType.INFANTRY
        if type_ == "cav":
            return CardType.CAVALRY
        if type_ == "art":
            return CardType.ARTILLERY

    @staticmethod
    def _build_card_deck(cards: list):
        card_deck = CardDeck(cards)
        return card_deck

    @staticmethod
    def _build_fix_card_sets(players) -> list:
        card_sets = [FixCardSet() for _ in players]
        return card_sets

    @staticmethod
    def _build_progressive_card_sets(players) -> list:
        reward_manager = CardRewardManager()
        card_sets = [ProgressiveCardSet(reward_manager) for _ in players]
        return card_sets

    @staticmethod
    def _build_card_managers(players: list, card_deck: CardDeck, card_sets: list):
        card_managers = dict()
        for player, card_set in zip(players, card_sets):
            card_manager = CardManager(card_deck, card_set, player)
            card_managers[player] = card_manager
        return card_managers

