from py_risk.builder.card_manager_builder import CardManagerBuilder
from py_risk.builder.map_builder import MapBuilder
from py_risk.builder.ui_builder import UIBuilder
from py_risk.builder.user_bar_builder import UserBarBuilder
from py_risk.game.GameState import GameState
from py_risk.game.game_logic.game_map import GameMap
from py_risk.game.game_logic.player_logic import PlayerLogic
from py_risk.game.game_logic.turn_logic.turn import Turn
from py_risk.game.view.game_map_gui import GameMapGUI
from py_risk.game_info import GameInformation


def build(game_info: GameInformation, ui_manager) -> GameState:
    map_builder = MapBuilder(game_info.map_size, game_info.player)
    game_map = GameMap(map_builder.get_territories(), map_builder.get_continents())
    card_manager_builder = CardManagerBuilder(map_builder.get_territories(), game_info.player, game_info)
    turn = Turn(game_info.player, game_map, card_manager_builder.get_card_managers())

    world = GameMapGUI(game_info.map_size, map_builder.get_territories_gui())
    user_bar = UserBarBuilder(game_info.map_size, game_info.window_size, ui_manager, turn).build_user_bar()
    ui = UIBuilder(ui_manager, turn, world, user_bar).get_ui()

    player_logic = PlayerLogic(game_info.player, game_map)

    game = GameState(ui, turn, player_logic, ui_manager)

    return game
