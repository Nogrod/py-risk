import os
import random

import yaml

from py_risk.game.game_logic.continent import Continent
from py_risk.game.game_logic.territory import Territory
from py_risk.game.game_logic.territory_image import TerritoryImage
from py_risk.game.view.game_map_gui import TerritoryGui
from py_risk.helper import load_image


class MapBuilder:

    def __init__(self, map_size: tuple, player: list):
        self.continents = []
        self.territories = dict()
        self.territories_gui = dict()

        self._build_map(map_size)
        self._assign_territories(player)

    def get_continents(self):
        return self.continents

    def get_territories(self):
        return self.territories

    def get_territories_gui(self):
        return self.territories_gui

    def _build_map(self, map_size):

        with open(os.path.join("data", "game_data", "map_data.yaml"), "r") as stream:
            map_data = yaml.safe_load(stream)

        self._build_territories(map_data["border_territories"], map_size)
        self._build_continents(map_data["continents"])

    def _add_border_territories(self, border_territory_data: dict):
        for territory, borders in border_territory_data.items():
            self.territories[territory].add_border_territories(self._territory_ids_to_territories(borders))

    def _territory_ids_to_territories(self, territories_ids: list) -> list:
        territories = [self.territories.get(id_) for id_ in territories_ids]
        return territories

    def _build_continents(self, continent_data: dict):
        for continent_name, data in continent_data.items():
            holding_bonus = data["holding_bonus"]
            territories_ids = data["territories_ids"]
            territories = self._territory_ids_to_territories(territories_ids)

            continent = Continent(name=continent_name, holding_bonus=holding_bonus, territories=territories)
            self.continents.append(continent)

    def _build_territories(self, territory_data: dict, map_size):
        territory_image_folder = os.path.join("data", "Maps")

        for territory_id in territory_data.keys():
            territory_path = os.path.join(territory_image_folder, f"{territory_id}.png".zfill(6))
            image = load_image(territory_path, map_size)
            territory_image = TerritoryImage(image)

            territory = Territory(territory_id, territory_image)
            territory_ui = TerritoryGui(territory_id, territory)
            self.territories[territory_id] = territory
            self.territories_gui[territory_id] = territory_ui

        self._add_border_territories(territory_data)

    def _assign_territories(self, players: list):
        territories = list(self.territories.values())
        random.shuffle(territories)

        for i, territory in enumerate(territories):
            player = players[i % len(players)]
            territory.switch_placer(player, 1)