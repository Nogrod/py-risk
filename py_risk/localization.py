import os
from enum import Enum

import yaml


class SupportedLanguages(Enum):
    ENGLISH = "english"
    GERMAN = "german"


localization = dict()


def init_localization(language: SupportedLanguages):
    localizations_path = os.path.join("data", "game_data", f"localization_{language.value}.yaml")

    with open(localizations_path, "r", encoding='UTF-8') as stream:
        localization_yaml = yaml.safe_load(stream)

    localization.update(localization_yaml["game_text"])
    localization.update(localization_yaml["players"])
    localization.update(localization_yaml["continents"])
    localization.update(localization_yaml["territories"])


def get_localization(element) -> str:
    text = localization.get(element, element)
    return text
