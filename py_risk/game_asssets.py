from enum import Enum

import pygame


class Color:
    CYAN = (85, 193, 175)
    ORANGE = (249, 164, 79)
    PURPLE = (72, 66, 154)
    RED = (173, 75, 64)
    BLUE = (63, 124, 255)
    GREEN = (76, 219, 83)

    PLAYER_COLORS = [
        CYAN,
        ORANGE,
        PURPLE,
        RED,
        BLUE,
        GREEN,
    ]

    PURE_RED = (255, 0, 0)
    PURE_BLUE = (0, 0, 255)
    PURE_GREEN = (0, 255, 0)

    BLACK = (0, 0, 0)
    WHITE = (255, 255, 255)
    GREY = (173, 173, 173)
    DARK_GREY = (50, 50, 50)


PlayerColorNameMapping = {
    Color.CYAN: "CYAN",
    Color.ORANGE: "ORANGE",
    Color.PURPLE: "PURPLE",
    Color.RED: "RED",
    Color.BLUE: "BLUE",
    Color.GREEN: "GREEN",
}


class RiskEventType:
    PY_RISK = 100
    TERRITORY_PRESSED = 101
    SOLIDER_COUNTER = 102
    NEXT_PHASE = 103
    SELECT_CARD = 104
    ACTIVATE_CARDS = 105


class Fonts:
    pygame.init()
    monospace_18 = pygame.font.SysFont("monospace", 18)
    monospace_20 = pygame.font.SysFont("monospace", 20)
    monospace_80 = pygame.font.SysFont("monospace", 80)
