import os

from py_risk import main

__version__ = "v2.2.8"
__author__ = 'Manuel Luther'
__licence__ = 'MIT License'

if __name__ == '__main__':
    os.chdir("py_risk")
    main.main()
