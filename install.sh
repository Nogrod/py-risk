sudo apt-get install python3
sudo apt-get -y install python3-venv
sudo apt -y install python3-pip

echo "========================================================================================"
echo "Installed Python and pip"
echo "create virtual enviroment"

python3 -m venv venv

echo "========================================================================================"
echo "install requirements"
./venv/bin/pip3 install -r "requirements.txt"
