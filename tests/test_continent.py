import unittest
from unittest.mock import Mock

from py_risk.game.game_logic.continent import Continent


class ContinentTests(unittest.TestCase):

    def test_full_holding_check_one_player(self):
        territories = []
        holding_bonus = 2

        player_mock = 0
        for i in range(20):
            territory_mock = Mock()
            territory_mock.player = player_mock
            territories.append(territory_mock)

        continent = Continent("Name", holding_bonus, territories)

        actual_holding_player_bonus = continent.get_player_holding_bonus(player_mock)

        expected_holding_player_bonus = holding_bonus

        self.assertEqual(expected_holding_player_bonus, actual_holding_player_bonus, 'There should be a holding player')

    def test_full_holding_check_multiple_player(self):
        territories = []

        player_mock_1 = 0
        for i in range(20):
            territory_mock = Mock()
            territory_mock.player = player_mock_1
            territories.append(territory_mock)

        player_mock_2 = 2
        territory_mock = Mock()
        territory_mock.player = player_mock_2
        territories.append(territory_mock)

        continent = Continent("Name", 2, territories)

        actual_holding_player_1 = continent.get_player_holding_bonus(player_mock_1)
        actual_holding_player_2 = continent.get_player_holding_bonus(player_mock_2)

        expected_holding_player_1_bonus = 0
        expected_holding_player_2_bonus = 0

        self.assertEqual(actual_holding_player_1, expected_holding_player_1_bonus, 'the value should be 0')
        self.assertEqual(actual_holding_player_2, expected_holding_player_2_bonus, 'the value should be 0')
