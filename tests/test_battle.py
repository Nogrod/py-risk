import unittest

from py_risk.game.game_logic.battle import Battle


class BattleTests(unittest.TestCase):

    def test_calculate_deaths_3_attacker_1_defender(self):
        attacker_dices = [6, 1, 1]
        defender_dices = [3]

        expected_attacker_deaths = 0
        expected_defender_deaths = 1

        self.general_calculate_deaths_test(attacker_dices, defender_dices, expected_attacker_deaths, expected_defender_deaths)

    def test_calculate_deaths_3_attacker_2_defender(self):
        attacker_dices = [6, 2, 1]
        defender_dices = [3, 2]

        expected_attacker_deaths = 1
        expected_defender_deaths = 1

        self.general_calculate_deaths_test(attacker_dices, defender_dices, expected_attacker_deaths, expected_defender_deaths)

    def test_calculate_deaths_2_attacker_2_defender(self):
        attacker_dices = [3, 3]
        defender_dices = [4, 3]

        expected_attacker_deaths = 2
        expected_defender_deaths = 0

        self.general_calculate_deaths_test(attacker_dices, defender_dices, expected_attacker_deaths, expected_defender_deaths)

    def test_calculate_deaths_1_attacker_2_defender(self):
        attacker_dices = [6]
        defender_dices = [5, 4]

        expected_attacker_deaths = 0
        expected_defender_deaths = 1

        self.general_calculate_deaths_test(attacker_dices, defender_dices, expected_attacker_deaths, expected_defender_deaths)

    def general_calculate_deaths_test(self, attacker_dices: list, defender_dices: list, expected_attacker_deaths: int, expected_defender_deaths: int):
        actual_attacker_deaths, actual_defender_deaths = Battle._calculate_deaths(attacker_dices, defender_dices)

        self.assertEqual(expected_attacker_deaths, actual_attacker_deaths, 'Calculate attacker deaths wrong')
        self.assertEqual(expected_defender_deaths, actual_defender_deaths, 'Calculate defender deaths wrong')