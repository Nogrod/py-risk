from unittest import TestCase

from py_risk.game.game_logic.card_logic.progressive_card_set import CardRewardManager


class TestCardRewardManager(TestCase):
    def test_get_current_reward_under_threshold(self):
        manager = CardRewardManager()

        manager.activation_counter = 3

        actual_value = manager.get_current_reward()

        expected_value = 8  # see risk rules

        self.assertEqual(expected_value, actual_value, 'Get wrong Value')

    def test_get_current_reward_over_threshold(self):
        manager = CardRewardManager()

        manager.activation_counter = 8

        actual_value = manager.get_current_reward()

        expected_value = 25  # see risk rules

        self.assertEqual(expected_value, actual_value, 'Get wrong Value')
