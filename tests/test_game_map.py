import unittest
from unittest.mock import Mock


from py_risk.game.game_logic.game_map import GameMap
from py_risk.game.game_logic.territory import Territory


class GameMapTests(unittest.TestCase):

    def test_check_player_territory_connection_case_with_connection(self):
        image_mock = Mock()
        player_mock = Mock()

        territory_source = Territory(0, image_mock)
        territory_1 = Territory(1, image_mock)
        territory_2 = Territory(2, image_mock)
        territory_target = Territory(3, image_mock)

        territory_source.add_border_territories(border_territories=[territory_1,territory_2])
        territory_1.add_border_territories(border_territories=[territory_target])

        territory_source.player = player_mock
        territory_1.player = player_mock
        territory_2.player = player_mock
        territory_target.player = player_mock

        actual_connection_result = GameMap.check_player_territory_connection(territory_source, territory_target)

        self.assertTrue(actual_connection_result, 'There should be a connection between the territories')

    def test_check_player_territory_connection_case_without_connection(self):
        image_mock = Mock()
        player_mock = Mock()

        territory_source = Territory(0, image_mock)
        territory_1 = Territory(1, image_mock)
        territory_2 = Territory(2, image_mock)
        territory_target = Territory(3, image_mock)

        territory_source.add_border_territories(border_territories=[territory_1, territory_2])
        territory_1.add_border_territories(border_territories=[territory_2])
        territory_2.add_border_territories(border_territories=[territory_source])

        territory_source.player = player_mock
        territory_1.player = player_mock
        territory_2.player = player_mock
        territory_target.player = player_mock

        actual_connection_result = GameMap.check_player_territory_connection(territory_source, territory_target)

        self.assertFalse(actual_connection_result, 'There should be NO connection between the territories')
