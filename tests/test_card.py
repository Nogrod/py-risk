import unittest

from py_risk.game.game_logic.card_logic.card_deck import CardDeck
from py_risk.game.game_logic.card_logic.fix_card_set import FixCardSet
from py_risk.game.game_logic.card_logic.card import Card, CardType, CardSoliderBonus


class CardDeckTests(unittest.TestCase):

    def test_draw_card(self):
        cards_in_deck = 10
        cards = [i for i in range(cards_in_deck)]

        shuffle_card_deck_mock = lambda x: x
        CardDeck._shuffle_card_deck = shuffle_card_deck_mock

        card_deck = CardDeck(cards)

        actual_card = card_deck.draw_card()
        actual_cards_in_deck = len(card_deck.card_deck)

        expected_card = 0
        expected_number_of_cards_in_deck = cards_in_deck - 1

        self.assertEqual(expected_card, actual_card, 'Draw wrong Card')
        self.assertEqual(expected_number_of_cards_in_deck, actual_cards_in_deck, 'Wrong number of cards in deck')

    def test_draw_last_card(self):
        number_used_cards = 3
        used_cards = [i for i in range(number_used_cards)]

        card_deck = CardDeck([number_used_cards])
        card_deck.used_cards = used_cards

        card_deck.draw_card()

        actual_cards_in_deck = len(card_deck.card_deck)

        expected_cards_in_deck = number_used_cards

        self.assertEqual(actual_cards_in_deck, expected_cards_in_deck)


class CardTests(unittest.TestCase):

    def test_equal_type(self):
        artillery_card_1 = Card(0, CardType.ARTILLERY)
        artillery_card_2 = Card(2, CardType.ARTILLERY)

        actual_solution = artillery_card_1.equal_type(artillery_card_2)

        self.assertTrue(actual_solution, 'Cards are not equal, but they should be equal')

    def test_equal_type_joker(self):
        artillery_card = Card(0, CardType.ARTILLERY)
        joker_card = Card(None, CardType.JOKER)

        actual_solution = artillery_card.equal_type(joker_card)

        self.assertTrue(actual_solution, 'Cards are not equal, but they should be equal')

    def test_equal_type_unequal(self):
        artillery_card = Card(0, CardType.ARTILLERY)
        infantry_ard = Card(1, CardType.INFANTRY)

        actual_solution = artillery_card.equal_type(infantry_ard)

        self.assertFalse(actual_solution, 'Cards are equal, but they should NOT be equal')


class CardSetTests(unittest.TestCase):

    def test_select_card(self):
        card_set = FixCardSet()

        card_to_select = Card(0, CardType.INFANTRY)

        card_set.toggle_card(card_to_select)

        actual_selected_cards = card_set.selected_cards

        expected_selected_cards = [card_to_select]

        self.assertListEqual(expected_selected_cards, actual_selected_cards, 'Wrong selected card')

    def test_select_card_multiple_selections(self):
        card_set = FixCardSet()

        cards_to_select = [Card(i, CardType.INFANTRY) for i in range(4)]

        for card in cards_to_select:
            card_set.toggle_card(card)

        actual_selected_cards = card_set.selected_cards

        expected_selected_cards = cards_to_select[:3]

        self.assertListEqual(expected_selected_cards, actual_selected_cards, 'Wrong selection of cards')

    def test_select_card_multiple_jokers(self):
        card_set = FixCardSet()

        cards_to_select = [Card(None, card_type) for card_type in [CardType.INFANTRY, CardType.JOKER, CardType.JOKER]]

        for card in cards_to_select:
            card_set.toggle_card(card)

        actual_selected_cards = card_set.selected_cards

        expected_selected_cards = cards_to_select[:2]

        self.assertListEqual(expected_selected_cards, actual_selected_cards, 'Maxium of one Joker can be selected')

    def test_unselect_card(self):
        card_set = FixCardSet()

        selected_cards = [Card(i, CardType.INFANTRY) for i in range(2)]

        card_set.selected_cards = selected_cards

        card_set._unselect_card(selected_cards[1])

        actual_selected_cards = card_set.selected_cards

        expected_selected_cards = [selected_cards[0]]

        self.assertListEqual(expected_selected_cards, actual_selected_cards, 'Unselect card went wrong')

    def test_get_same_card_type_with_joker(self):
        card_set = FixCardSet()

        selected_cards = [Card(i, CardType.INFANTRY) for i in range(2)]
        joker_card = Card(None, CardType.JOKER)
        selected_cards.insert(0, joker_card)

        card_set.selected_cards = selected_cards

        actual_card_type = card_set._get_same_card_type()

        expected_card_type = CardType.INFANTRY

        self.assertEqual(expected_card_type, actual_card_type, 'Get a wrong CardType')

    def test_update_card_value_same_cards(self):
        card_set = FixCardSet()

        selected_cards = [Card(i, CardType.INFANTRY) for i in range(3)]

        card_set.selected_cards = selected_cards

        card_set._update_set_troop_value()

        actual_card_value = card_set.set_troop_value

        expected_troop_value = CardSoliderBonus.INFANTRY.value

        self.assertEqual(expected_troop_value, actual_card_value, 'Calculate wrong card set value')

    def test_update_card_value_different_cards(self):
        card_set = FixCardSet()

        selected_cards = [Card(0, CardType.INFANTRY), Card(1, CardType.CAVALRY), Card(2, CardType.ARTILLERY)]

        card_set.selected_cards = selected_cards

        card_set._update_set_troop_value()

        actual_card_value = card_set.set_troop_value

        expected_troop_value = CardSoliderBonus.DIFFERENT.value

        self.assertEqual(expected_troop_value, actual_card_value, 'Calculate wrong card set value')

    def test_update_card_value_different_cards_with_joker(self):
        card_set = FixCardSet()

        selected_cards = [Card(0, CardType.INFANTRY), Card(1, CardType.CAVALRY), Card(2, CardType.JOKER)]

        card_set.selected_cards = selected_cards

        card_set._update_set_troop_value()

        actual_card_value = card_set.set_troop_value

        expected_troop_value = CardSoliderBonus.NONE.value

        self.assertEqual(expected_troop_value, actual_card_value, 'Calculate wrong card set value')


