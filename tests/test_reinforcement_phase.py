import unittest
from unittest.mock import Mock

from py_risk.game.game_logic.turn_logic.reinforcement_phase import ReinforcementPhase


class ReinforcementPhaseTests(unittest.TestCase):

    def test_calculate_reinforcement_quantity(self):
        player_territory_quantity = 8
        player_continent_bonus = 3

        expected_reinforcement_quantity = 5

        game_map_mock = Mock()
        game_map_mock.get_player_territory_quantity.return_value = player_territory_quantity
        game_map_mock.get_player_continent_bonus.return_value = player_continent_bonus

        # a specific player or card_manager object is not needed. Any value is ok
        player_mock = 0
        card_manager_mock = 0

        phase = ReinforcementPhase(player_mock, game_map_mock, card_manager_mock)

        actual_reinforcement_quantity = phase.calculate_reinforcement_quantity()

        self.assertEqual(expected_reinforcement_quantity, actual_reinforcement_quantity, "Reinforcement Quantity get calculated wrong")

    def test_calculate_reinforcement_quantity_under_limit(self):
        player_territory_quantity = 1
        player_continent_bonus = 0

        expected_reinforcement_quantity = 3

        game_map_mock = Mock()
        game_map_mock.get_player_territory_quantity.return_value = player_territory_quantity
        game_map_mock.get_player_continent_bonus.return_value = player_continent_bonus

        # a specific player or card_manager object is not needed. Any value is ok
        player_mock = 0
        card_manager_mock = 0

        phase = ReinforcementPhase(player_mock, game_map_mock, card_manager_mock)

        actual_reinforcement_quantity = phase.calculate_reinforcement_quantity()

        self.assertEqual(expected_reinforcement_quantity, actual_reinforcement_quantity, "Reinforcement Quantity get calculated wrong.")
