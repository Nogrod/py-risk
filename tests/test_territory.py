import os
import unittest

import pygame

from py_risk.game.game_logic.territory_image import TerritoryImage
from py_risk.helper import load_image


class TerritoryImageTests(unittest.TestCase):

    def test_change_color(self):
        pygame.init()  # needed to load the image probably
        window_surface = pygame.display.set_mode((1, 1))  # need to init a surface, else pygame crashes

        image_path = "test_image.png"
        if image_path not in os.listdir("."):
            image_path = "tests/test_image.png"

        image = load_image(image_path, target_image_size=(100,100))
        territory = TerritoryImage(image)

        target_color = (155, 155, 155)

        territory.change_color(target_color)

        actual_image = territory.image

        for x in range(0, territory.bounding_rect.width):
            for y in range(0, territory.bounding_rect.height):
                actual_color = actual_image.get_at((territory.bounding_rect.x + x, territory.bounding_rect.y + y))[:3]
                if image.get_at((territory.bounding_rect.x + x, territory.bounding_rect.y + y)) == (0, 0, 0):
                    expected_color = (0, 0, 0)
                    self.assertTupleEqual(expected_color, actual_color, 'Colored a point where should be no color')
                else:
                    expected_color = target_color
                    self.assertTupleEqual(expected_color, actual_color, 'Colored a point in a wrong color')

        pygame.quit()
